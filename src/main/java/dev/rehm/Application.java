package dev.rehm;

import dev.rehm.daos.ActorDao;
import dev.rehm.daos.ActorDaoImpl;
import dev.rehm.daos.MovieDao;
import dev.rehm.daos.MovieDaoImpl;
import dev.rehm.models.Actor;
import dev.rehm.models.Movie;
import dev.rehm.util.ConnectionUtil;

import java.sql.*;
import java.time.LocalDate;
import java.util.*;

public class Application {


    public static void main(String[] args) {
//        ActorDao actorDao = new ActorDaoImpl();
//
//        Actor actor = new Actor("Idris Elba", LocalDate.of(1972, 9, 6));
//        boolean result = actorDao.addNewActor(actor);
//        System.out.println(result);
//
//
//        List<Actor> actors = actorDao.getAllActors();
//        for(Actor a: actors){
//            System.out.println(a);
//        }

//        System.out.println(getAccountBalances());

//        MovieDaoImpl movieDao = new MovieDaoImpl();
//        List<Movie> movies = movieDao.getAllMoviesMapSolution();
//        for(Movie m: movies){
//            System.out.println(m);
//        }

        ActorDaoImpl actorDao = new ActorDaoImpl();
        Actor newActor = new Actor("Elijah Wood", LocalDate.of(1981, 1, 28));
        System.out.println(newActor);
        Actor addedActor = actorDao.addNewActorFunction(newActor);
        System.out.println(addedActor);

    }

    /*
    we used this method to try accessing objects in another schema
     */
    public static List<Double> getAccountBalances(){
        try (Connection c = ConnectionUtil.getConnection();
             Statement s = c.createStatement();){
            List<Double> balances = new ArrayList<>();

            ResultSet rs = s.executeQuery("select * from account");
            while(rs.next()){
                balances.add(rs.getDouble("balance"));
            }
            return balances;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public List<Movie> getAllMovies(){
    String sql = "select m.id, movie_name, release_year, duration, actor_id, actor_name, birthdate" +
            " from {oj movie m left join movie_actor ma on m.id = ma.movie_id " +
            "left join actor a on a.id = ma.actor_id }";
        try(Connection connection = ConnectionUtil.getConnection();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(sql)){

            List<Movie> movies = new ArrayList<>();
            // currentMovie keeps track of the movies as we go through them, represents the previous movie when the
            // while loop executes again
            Movie currentMovie = null;
            while(rs.next()){
                int newId = rs.getInt("id");

                // if the new id from the result set does not have the same id as the previous movie, we need to
                // create a new movie object and add it to the list of movies
                if(currentMovie==null || newId!=currentMovie.getId()){
                    //create new movie
                    currentMovie = new Movie();
                    currentMovie.setId(newId);
                    currentMovie.setName(rs.getString("movie_name"));
                    currentMovie.setReleaseYear(rs.getInt("release_year"));
                    currentMovie.setDuration(rs.getInt("duration"));
                    movies.add(currentMovie);
                }

                // whether or not the movie has been added in this iteration, we'll associate the current actor with
                // the most recent movie, if it does in fact have an actor associated with it
                int actorId = rs.getInt("actor_id");
                if(actorId!=0) {
                    String name = rs.getString("actor_name");
                    LocalDate date = rs.getObject("birthdate", LocalDate.class);
                    Actor newActor = new Actor(actorId, name, date);
                    if(currentMovie.getActorList()==null){
                        currentMovie.setActorList(new ArrayList<>());
                    }
                    currentMovie.getActorList().add(newActor);
                }

            }
            return movies;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
}

}
