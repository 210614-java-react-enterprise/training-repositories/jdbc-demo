package dev.rehm.daos;

import dev.rehm.util.ConnectionUtil;
import org.checkerframework.checker.units.qual.A;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ActorDaoImplTest {

    ActorDao actorDao = new ActorDaoImpl();

    @BeforeAll
    public static void runSetup() throws SQLException, FileNotFoundException {
        try(Connection connection = ConnectionUtil.getConnection()){
            RunScript.execute(connection, new FileReader("setup.sql"));
        }
    }

    @Test
    public void getAllActorsTestNotNull(){
        // check if what we get back from the db is null
        assertNotNull(actorDao.getAllActors());
    }

    @Test
    public void getAllActorsTestValidNumberOfRecords(){
        // check if we get the right amount of actors back from the db
        assertEquals(3, actorDao.getAllActors().size());
    }

    @AfterAll
    public static void runTeardown() throws SQLException, FileNotFoundException {
        try(Connection connection = ConnectionUtil.getConnection()){
            RunScript.execute(connection, new FileReader("teardown.sql"));
        }
    }

}
