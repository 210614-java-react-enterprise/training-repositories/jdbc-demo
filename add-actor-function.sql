create function add_actor(actor_name_input actor.actor_name%type, actor_birthday_input actor.birthdate%type)
returns setof actor
language plpgsql
as $$
declare 
	new_id integer;
begin
	insert into actor values (default, actor_name_input, actor_birthday_input);
	
	select last_value into new_id 
	from actor_id_seq;

	return query select * from actor where id = new_id;	
end
$$

select add_actor('Samuel L. Jackson', '1948-12-21');