create table movie(
	id serial primary key,
	movie_name varchar(100),
	release_year integer,
	duration integer
);

create table actor(
	id serial primary key,
	actor_name varchar(100),
	birthdate date
);

create table movie_actor(
	movie_id integer references movie,
	actor_id integer references actor,
	primary key(movie_id, actor_id)
);


insert into actor (actor_name) values ('Daniel Day-Lewis');
insert into actor (actor_name) values ('Mark Hamill');
insert into actor (actor_name) values ('Kate Winslet');

